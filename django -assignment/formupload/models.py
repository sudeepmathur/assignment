from __future__ import unicode_literals

from django.db import models

# Create your models here.
class TimeStampedModel(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True

# To-do create different model for year and particulars
# and have a key reference
class Values(TimeStampedModel):
	name = models.CharField(max_length=30,default='abc')
	year = models.IntegerField()
	value = models.FloatField()