# move this file to helper module

import pdb
from tabula import read_pdf, convert_into
import os
import csv
from pandas import *
import StringIO
from models import Values
import math

def cleanData(data,location):
	#to-do ask user whether data is clean or not
	headers = ['Particulars','2015','2016','Particulars_1','','2015_1','2016_1']
	split_data = data['2016 Particulars'].str.split(' ',1).str
	data['2016 Particulars'] = split_data[0]
	data['Unnamed: 3'] = split_data[1]
	data.columns = headers
	# to-do get name of the original file
	data.to_csv(location+'/output1.csv', columns = headers,index=False)
	return data

def queryData(year,variable):
	value = None
	try:
		obj = Values.objects.get(name=variable,year=int(year))
		return obj.value
	except:
		return value

def parseData(dataframe):
	for column in dataframe:
		if column in ['Particulars', 'Particulars_1']:
			if column == 'Particulars':
				index=0
				while index < dataframe.index._stop:
					particular = dataframe[column][index]
					if particular == particular:
						if dataframe['2015'][index] == dataframe['2015'][index] :
							obj, success = Values.objects.get_or_create(name=particular,year=2015,value=dataframe['2015'][index])
						if dataframe['2016'][index] == dataframe['2016'][index]:
							obj, success = Values.objects.get_or_create(name=particular,year=2016,value=dataframe['2016'][index])
						print index
					index = index+1

			if column == 'Particulars_1':
				index=0
				while index < dataframe.index._stop:
					particular = dataframe[column][index]
					if particular == particular:
						if dataframe['2015_1'][index] == dataframe['2015_1'][index]:
							obj, success = Values.objects.get_or_create(name=particular,year=2015,value=dataframe['2015_1'][index])
						if dataframe['2016_1'][index] == dataframe['2016_1'][index]:
							obj, success = Values.objects.get_or_create(name=particular,year=2016,value=dataframe['2016_1'][index])
					index = index+1
		

def convert_to_dataframe(location,file):
	# to-do delete the temp file
	dataframe = convert_into(file, "file.csv",output_format="csv")
	data = read_csv('file.csv')
	return data



#obj = PdfToCsv()

