from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import UploadFileForm
import pdb
from django.core.files.storage import FileSystemStorage
from script import convert_to_dataframe,cleanData,parseData,queryData

def home(request):
	return render(request, 'index.htm', {'what':'File Upload'})

def upload_file(request):
	if request.method == 'POST' and request.FILES:
		myfile = request.FILES['file']
		fs = FileSystemStorage()
		# save file to media storage
		filename = fs.save(myfile.name, myfile)
		uploaded_file_url = fs.url(filename)
		form = UploadFileForm(request.POST, request.FILES)
		handle_uploaded_file(fs.location, fs.location+'/'+filename)
		# To-do check the year
		query_year = request.POST.get('year','')
		query_variable = request.POST.get('variable','')
		value = queryData(query_year,query_variable)
		return render(request,'download.htm',{'variable':query_variable,'year':query_year,'value':value})
		# to-do generate download link of the csv file
	else:
		form = UploadFileForm()
	return render(request, 'index.htm', {'form': form})

def handle_uploaded_file(location, f):
	data = convert_to_dataframe(location, f)
	dataframe = cleanData(data,location)
	parseData(dataframe)

def download_file(request):	
	#to-do generate the download link to the file
	pass