import pdb
from tabula import read_pdf, convert_into
import os
import csv
from pandas import *

class PdfToCsv(object):

	def __init__(self):
		super(PdfToCsv, self).__init__()
		self.getPdfFile()

	def cleanData(self,data):
		# To-do ask user whether data is clean or not
		headers = ['Particulars','2015','2016','Particulars','2015','2016']
		# To-do ask for columns to split
		split_data = data['2016 Particulars'].str.split(' ',1).str
		data['2016 Particulars'] = split_data[0]
		data['Unnamed: 3'] = split_data[1]
		# To-do get name of the file and save to original location
		data.to_csv('output1.csv', columns = headers,index=False)
		
	def getPdfFile(self):
		# To-do move the non-core functionality to helper function
		file = raw_input("Enter the full file path ")
		# file = "/home/tech-4/Downloads/Parsing Test for Hiring/Parsing Test for Hiring/BalSheet.pdf"
		if os.path.isfile(file):
			dataframe = convert_into(file, "file.csv",output_format="csv")
			#to-do  delete the temp file
			data = read_csv('file.csv')
			# to-do ask user whether data is clean or not
			self.cleanData(data)
		else:
			print 'file does not exists at the given path'


obj = PdfToCsv()

